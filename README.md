**How main script works**

    • One by one processes plane images in a folder; 
    • read a corresponding json file with bounding boxes;
    • cut a plane image into a images with string based on bounding boxes and save it;
    • run calamari to predict strings in cropped images; calamari saves the results to corresponding txt files;
    • crop string images into separate words using information about predicted phrase: number of words and white spaces;
    • save new json files with predicted words and corresponding bounding boxes;
    • take next image. 

** Install and run:**

    • Is quite easy: conda env create -f environment_master_gpu.yml
    • Calamary version 1.x doesn’t work properly and returns scaring errors deep down in functions and subroutines. 
    • We used Calamari version 0.3.x:
      Conda env create -f  /home/dzyga/Downloads/calamari-0.3.3/environment_master_cpu.yml
    • Both with CPU and GPU.
    • Download pretrained models from: https://github.com/Calamari-OCR/calamari_models 
    • Run from terminal: calamari-predict --checkpoint path_to_model.ckpt --files your_images.*.png 
    • --checkpoint path_to_model.ckpt  -> instead pass a .json (in the folder with pretrained models) file. 
    • To execute all pictures in folder:  
       calamari-predict --checkpoint /home/dzyga/My/Python/ITJim/Projects/OCR/calamari/calamari_models-1.0/antiqua_modern/4.ckpt.json  --files /home/dzyga/My/Python/ITJim/Projects/OCR/benchmark_20/croped_images/*.png
	   
**Calamari sources:**
https://github.com/Calamari-OCR/calamari 
https://arxiv.org/pdf/1807.02004.pdf