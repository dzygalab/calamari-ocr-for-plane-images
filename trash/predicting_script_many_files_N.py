import subprocess
import os
from calamari_planeimage import crop_planeimage, txt_to_json, split_json_strings

root_path = '/home/dzyga/My/Python/ITJim/Projects/OCR/plane_images/'

plan_img_folder = 'good gt images/'
#json_folder = 'chars_words_strings_11.12/'

# new json_folder with no words inside 
json_folder = 'letters_and_strings/'


# loop throug all images
all_imgs = (f for f in os.listdir(root_path + plan_img_folder) if f.endswith('.png'))
#print('Found {} plane images in a folder'.format(len(all_imgs)))

for im in all_imgs: 
    # 1. preparing names of files
    image_no = im[:-4]

    print(""" 
----------------------------------
    Reading new image No {}
    """.format(image_no))

    plan_img_name = image_no + '.png' # like '3_1.png'
    json_name = image_no + '_strings' + '.json' # like '3_1_strings.json' # _words

    plan_img_file = root_path + plan_img_folder + plan_img_name
    json_file = root_path + json_folder + json_name

    # new folders creating
    txt_folder = root_path + 'croped_images_' + image_no +'/' # like 'croped_images_3_1'
    print('Creating folder {}'.format(txt_folder))
    
#     if not os.path.exists(txt_folder):
#             os.makedirs(txt_folder)

    res_folder = root_path + 'calamari_result/' 
    if not os.path.exists(res_folder):
            os.makedirs(res_folder)

    # 2. cropping a plane image into boxes with strings
    print("""
    Reading a plane image and cropping it fot bounding boxes with text
    Plane image ---> {}
    """.format(plan_img_name))

    # idx - indeces in the middle of the white space
    gt_boxes, index, width = crop_planeimage(json_file, plan_img_file, txt_folder)

    # 3. predicting text with calamari model
    print("""
    Runing calamari prediction """)
    PATH_CALAMARI = '/home/dzyga/My/Python/ITJim/Projects/OCR/calamari/'
    PATH_TO_MODELS = PATH_CALAMARI + 'calamari_models-1.0/antiqua_modern/4.ckpt.json'
    # in case of several voting models
    # PATH_TO_MODELS = PATH_CALAMARI + 'calamari_models-1.0/antiqua_modern/0.ckpt.json', PATH_CALAMARI + 'calamari_models-1.0/antiqua_modern/1.ckpt.json'
    #                         #        + 'calamari_models-1.0/antiqua_modern/2.ckpt.json'
    #                         #        + 'calamari_models-1.0/antiqua_modern/3.ckpt.json'
    #                         #        + 'calamari_models-1.0/antiqua_modern/4.ckpt.json'

    PATH_TO_IMAGES = txt_folder + '*.png'
    print("""
    Predicting for images from ---> 
    {}
    """.format(PATH_TO_IMAGES))

    calamari_script = 'calamari-predict'
    subprocess.run([calamari_script, '--checkpoint', PATH_TO_MODELS, '--files', PATH_TO_IMAGES])

    # 4. writing new json files with predictions
    json_output = 'predicted_' + json_name # like predicted_1_21_words.json 
    print("""
    Writing json file with predicions
    {}
    """.format(json_output))
    txt_to_json(gt_boxes, plan_img_name, txt_folder, res_folder, json_output)

    
    # 5. deleting unnecessary folders 
    print('! Deleting a folder {} !'.format(txt_folder))
    subprocess.call(['rm', '-rf', txt_folder])


    # 6. splitting strings into separate words 
    # ... going to be here
    in_json_f = res_folder + json_output
    out_json_f = root_path + 'strings_to_words/' + 'split_' + json_output  

    if not os.path.exists(root_path + 'strings_to_words/'):
        os.makedirs(root_path + 'strings_to_words/')
        
    split_json_strings(in_json_f, out_json_f, index, width)
    
    # 7. crop plane image into separate words
    splited_folder = root_path + 'word_images_' + image_no + '/'
    crop_planeimage(out_json_f, plan_img_file, splited_folder)