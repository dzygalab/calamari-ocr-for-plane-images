import subprocess
import os
from calamari_planeimage_v1 import crop_planeimage, txt_to_json, split_json_strings

root_path = '/home/dzyga/My/Python/ITJim/Projects/OCR/plane_images/'

plan_img_folder = 'good gt images/'
image_no = '1_16'
plan_img_name = image_no + '.png' # like '3_1.png'

json_folder = 'letters_and_strings/'
json_name = image_no + '_strings' + '.json' # like '3_1_strings.json' # _words

plan_img_file = root_path + plan_img_folder + plan_img_name
json_file = root_path + json_folder + json_name

# new folders creating
txt_folder = root_path + 'croped_images_' + image_no +'/' # like 'croped_images_3_1'

res_folder = root_path + 'result_test/' 
if not os.path.exists(res_folder):
        os.makedirs(res_folder)

print('Reading a plane image and cropping it with bounding boxes')
string_boxes, word_boxes = crop_planeimage(json_file, plan_img_file, txt_folder)

"""
print(' ')
print('Runing calamari prediction')
PATH_CALAMARI = '/home/dzyga/My/Python/ITJim/Projects/OCR/calamari/'
PATH_TO_MODELS = PATH_CALAMARI + 'calamari_models-1.0/antiqua_modern/4.ckpt.json'
# in case of several voting models
# PATH_TO_MODELS = PATH_CALAMARI + 'calamari_models-1.0/antiqua_modern/0.ckpt.json', PATH_CALAMARI + 'calamari_models-1.0/antiqua_modern/1.ckpt.json'
#                         #        + 'calamari_models-1.0/antiqua_modern/2.ckpt.json'
#                         #        + 'calamari_models-1.0/antiqua_modern/3.ckpt.json'
#                         #        + 'calamari_models-1.0/antiqua_modern/4.ckpt.json'

PATH_TO_IMAGES = txt_folder + '*.png'
print('Predicting for images from --->')
print(PATH_TO_IMAGES)
print(' ')

calamari_script = 'calamari-predict'
subprocess.run([calamari_script, '--checkpoint', PATH_TO_MODELS, '--files', PATH_TO_IMAGES])


print(' ')
print('Writing json file with predicions')
json_output = 'predicted_' + json_name # like predicted_1_21_words.json 
print(json_output)
write_json_from_txt(gt_boxes, plan_img_name, txt_folder, res_folder, json_output)

print('Deleting a folder with croped images !!!')
subprocess.Popen(['rm', '-rf', txt_folder])"""