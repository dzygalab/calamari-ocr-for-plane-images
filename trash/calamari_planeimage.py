import numpy as np
import cv2
import json
import os

def crop_planeimage(json_gt_file, plan_img_file, txt_folder):
    """10/12/19 by N.
        Crop one image into many with text lines based on json file with ground truth 
        labels and bounding boxes.
        Save images
    """

    if not os.path.exists(txt_folder):
        os.makedirs(txt_folder)
    
    plane_img = cv2.imread(plan_img_file)
    
    imh = plane_img.shape[0]
    imw = plane_img.shape[1]
        
    print('Image shape ---> {}x{}'.format(imh, imw))
    
    with open(json_gt_file, "r") as file:
        text_regions = json.load(file)
    
    print('Found {} bounding boxes'.format(len(text_regions['TextRegions'])))
        
    gt_boxes = {}
    index = []
    width = []
    for i, box in enumerate(text_regions['TextRegions']):
        label = (box['Text'])
        # if label != -1: ---- >>> error ?!
        x = int(box['Region']['X']*imw)
        y = int(box['Region']['Y']*imh)
        w = int(box['Region']['W']*imw)
        h = int(box['Region']['H']*imh)
            
        # extract dict from json file 
        new_detection = [box['Region']['X'],
                        box['Region']['Y'], 
                        box['Region']['W'],
                        box['Region']['H']]# , label]
        
        gt_boxes[str(i)] = new_detection
                    
        # image 
        text_box = plane_img[y-1:y+h+2, x-1:x+w+2, :].copy()
        if text_box.size > 1:
            # make white border 
            text_box_wider = cv2.copyMakeBorder(text_box, 2, 2, 2, 2, 
                            borderType=cv2.BORDER_CONSTANT, value=[255,255,255])
            cv2.imwrite(txt_folder + str(i) + '.png', text_box_wider)

            # cutting string in image into wirds  
            # idx - indeces in the middle of the white space
            idx, wdh = split_image_string(text_box[:,:,0], imw) 
            index.append(idx)
            width.append(wdh)  
        else:
            print('This text box looks weird: {}'.format())
    
    print('Saved croped images in {}'.format(txt_folder))           
    print('Number of bounding boxes found ---> {}'.format(len(gt_boxes)))
    return gt_boxes, index, width


def txt_to_json(gt_boxes, plan_img_name, txt_folder, res_folder, 
                        json_output_name = 'predicted.json'):
    """ 10/12/19 by N. 
        Read many txt files with calamari predictions and write one json file 
        which includes predicted text and bounding boxes of the original plane image.
        Boundin boxes 'gt_boxes' are taken from another json file with ground truth labels
    """
    # print(gt_boxes['0'])
    # print(' ')
    
    text_regions = []
    predicted_txt = [f for f in os.listdir(txt_folder) if f.endswith('.txt')]
    print('Number of predicted words ---> {}'.format(len(predicted_txt)))

    cnt_blank = 0 # has no text inside
    for file in predicted_txt:
        with open(txt_folder + file, 'r') as f:
            key = file[:-9] # number of detection
            word = f.readline()
            if word == 'NETGEAR AGM732F':
                print('fucking word AGM is here {}'.format(key))
            if len(word)<1:
                cnt_blank += 1
                # print('The {}st prediction is blank'.format(key))
                # print('')

            x = gt_boxes[key][0]
            y = gt_boxes[key][1]
            w = gt_boxes[key][2]
            h = gt_boxes[key][3]

            text_regions.append({"Text": word,
                                     "Region": {
                                         "X": x,
                                         "Y": y,
                                         "W": w,
                                         "H": h }
                                     })
    
    results_for_json = {"TextRegions": text_regions}
    json_output_file = res_folder + json_output_name
    jstr = json.dumps(results_for_json, indent=2)

    with open(json_output_file, 'wt') as outfile:
        for s in jstr:
            outfile.write(s)
    print('Found {} boxes with no text'.format(cnt_blank))
    print('Wrote to {}'.format(json_output_file))


def split_image_string(im_string, imw):
    """ 19/12/19 by N.
        im_string - image with text to split; must be in gray scale
        imw - plane image width
    """
    _, im_string_g = cv2.threshold(im_string, 150, 255, cv2.THRESH_BINARY)
    text_line = (255-im_string_g).sum(axis = 0)
    text_line = text_line / text_line.max()

    text_line[text_line>0.06] = 1 
    text_line[text_line<0.5] = 0

    xxx = np.arange(len(text_line))
    z_line = np.zeros(len(text_line))

    spaces = xxx[text_line==0]
    
    b = np.zeros(1) # a kind of histogram
    k = 0 # index of histogram
    j = np.zeros(1, dtype=int) # index where white space starts
    for i in range(len(spaces)-1):
        if spaces[i+1] - spaces[i] == 1:
            b[k] += 1 # new item in a bin  
        else:
            j = np.append(j, spaces[i+1])
            b = np.append(b, 1) # move to new bin 
            k += 1 #  

    b12 = (b/2).astype(int)
    ind_all = j+b12 # indeces moved to half up
    ind_largest = np.argsort(b)[::-1]
    idx = ind_all[ind_largest]
    
    return idx/imw, im_string_g.shape[1]/imw # arb.units


def split_json_strings(in_json, out_json, index, width):
    """ 17/12/19 by N.
    """

    with open(in_json, "r") as file:
        text_regions = json.load(file)

    len_original = len(text_regions['TextRegions']) 

    new_boxes = []
    cnt_w = 0
    cnt_s = 0
    for box in text_regions['TextRegions']:
        string = box['Text']
        x = box['Region']['X']
        y = box['Region']['Y']
        w = box['Region']['W']
        h = box['Region']['H']

        string_list = string.split(' ')
        if len(string_list) > 1:
            # lpc = w/len(string) # length per char
            # w_word = [len(s) * lpc + lpc for s in string_list] 
            # w_word = np.asarray(w_word)
            # x_word = x + np.cumsum(w_word) - w_word[0]

            N = len(string_list)
            if len(index) > N:
                index = index[:N] # N main white spaces 
            idx = np.sort(index)
            idx = np.insert(idx, 0, 0) # now index starts from the 0 
            idx = np.append(idx, width) # add the last index
            x_word = x + idx
            k = np.arange(len(idx)-1) 
            w_word = idx[k+1]-idx[k]
            
            # split the string!
            #delta = lpc # add some space 
            for i, word in enumerate(string_list):
                x1 = x_word[i]
                y1 = y
                w1 = w_word[i]
                h1 = h
            
                new_box = {}
                new_box['Text'] = word
                new_box['Region'] = {}
                new_box['Region']['X'] = x1
                new_box['Region']['Y'] = y1
                new_box['Region']['W'] = w1
                new_box['Region']['H'] = h1
                
                #print(new_box)
                new_boxes.append(new_box)
                cnt_w += 1 # added one more word
            cnt_s += 1 # splitted one more string 
        else:
            new_boxes.append(box)
            
    
    print('Added {} words'.format(cnt_w))
    print('Remove {} strings'.format(cnt_s))
    print('The length was {}'.format(len_original))
    print('Now it should be: ')
    print('{} - {} + {} = {}'.format(len_original, cnt_s, cnt_w, len(new_boxes)))
    
    
    # save new json file
    new_text_regions = {}
    new_text_regions['TextRegions'] = new_boxes
    
    
    jstr = json.dumps(new_text_regions, indent=2)
    with open(out_json, 'wt') as outfile:
        for s in jstr:
            outfile.write(s)
        print('Wrote to {}'.format(out_json))
    
    return new_boxes




def main():
    root_path = '/home/dzyga/My/Python/ITJim/Projects/OCR/plane_images/'

    image_no = '1_16' # !!!!! 
    plan_img_folder = 'good gt images/'
    plan_img_name = image_no + '.png' # like '3_1.png'
    plan_img_file = root_path + plan_img_folder + plan_img_name

    # check a function wich cuts string into words 
    json_folder = 'calamari_result/'
    json_name = 'predicted_' + image_no + '_strings' + '.json'

    splited_folder = root_path + 'splited_strings_images_' + image_no +'/' # like 'croped_images_3_1'
    if not os.path.exists(splited_folder):
        os.makedirs(splited_folder)
 
    #  1. 
    in_json_f = root_path + json_folder + json_name
    out_json_f = root_path + 'strings_to_words/' + 'split_' + json_name
    
    print("""
    Split strings from 
    {}
    into
    {}
    """.format(in_json_f, out_json_f))
    
    json_words_file = split_json_strings(in_json_f, out_json_f)
    
    # 2.
    
    print("""
    Reading a plane image and cropping it with bounding boxes 
    """)
    
    gt_boxes = crop_planeimage(out_json_f, plan_img_file, splited_folder)
    
        


if __name__ == '__main__':
    main()
