""" 19/12/19 by N.
"""
import numpy as np
import cv2
import json
import os
import subprocess

from planeimage_functions import cut_image_with_json, json_with_predicted_words, split_image_string


def predict_one_file(root_path, image_no):
    # Sources 
    #root_path = '/home/dzyga/My/Python/ITJim/Projects/OCR/plane_images/'
    plan_img_folder = 'good gt images/'
    plan_img_name = image_no + '.png' # like '3_1.png'
    plan_img_file = root_path + plan_img_folder + plan_img_name

    json_folder = 'letters_and_strings/'
    json_name = image_no + '_strings' + '.json' # like '3_1_strings.json' # _words
    json_file = root_path + json_folder + json_name

    # result folders 
    result_folder = root_path + 'calamari_result/' # everything is here 
    images_folder = result_folder + image_no + '/' # croped string images are here
    words_folder = images_folder + 'words/' # croped word images are here
    if not os.path.exists(result_folder):
        os.makedirs(result_folder)
    if not os.path.exists(images_folder):
        os.makedirs(images_folder)
    if not os.path.exists(words_folder):
        os.makedirs(words_folder)
        # json with words
    json_words_folder = result_folder + 'json_words/' 
    if not os.path.exists(json_words_folder):
        os.makedirs(json_words_folder)

    # calamari 
    #PATH_CALAMARI = '/home/dzyga/My/Python/ITJim/Projects/OCR/calamari/'
    PATH_TO_MODELS = root_path + 'calamari_models-1.0/antiqua_modern/4.ckpt.json'
    PATH_TO_IMAGES = images_folder + '*.png'
    calamari_script = 'calamari-predict'


    # 1. crop plane image into text boxes
    print(' ')
    print('--- Open plane image No {} ---'.format(image_no))
    print(' ')

    plane_img = cv2.imread(plan_img_file)
    imh = plane_img.shape[0]
    imw = plane_img.shape[1]
   
    string_boxes = cut_image_with_json(json_file, images_folder, plane_img, imw, imh)

    # 2. calamari
    subprocess.run([calamari_script, '--checkpoint', PATH_TO_MODELS, '--files', PATH_TO_IMAGES])

    # 3. read predicted string
    word_regions = json_with_predicted_words(images_folder, words_folder, string_boxes, imw, imh)    
    
    # 4. write new json
    json_output_file = json_words_folder + json_name
    results_for_json = {"TextRegions": word_regions}

    jstr = json.dumps(results_for_json, indent=2)
    with open(json_output_file, 'wt') as outfile:
        for s in jstr:
            outfile.write(s)

    print('New json has {} words'.format(len(word_regions)))
    print('Wrote new json to {}'.format(json_output_file))


if __name__ == "__main__":
    import timeit        
    
    start = timeit.default_timer()
    # loop throug all images
    root_path = '/home/dzyga/My/Python/ITJim/Projects/OCR/plane_images/'
    plan_img_folder = 'good gt images/'
    all_imgs = (f for f in os.listdir(root_path + plan_img_folder) if f.endswith('.png'))

    # one particular file
    # image_no = '1_16'
    # predict_one_file(image_no)

    # all imaages in folder 
    for im in all_imgs: 
        image_no = im[:-4]
        predict_one_file(root_path, image_no)

    stop = timeit.default_timer()
    print("""Prediction took 
             {} mins""".format((stop - start)/60))
