import numpy as np
import cv2
import json
import os

def cut_image_with_json(json_file, images_folder, plane_img, imw, imh):
    with open(json_file, "r") as file:
            text_regions = json.load(file)

    string_boxes = {}
    for i, box in enumerate(text_regions['TextRegions']):
        X = box['Region']['X'] # a.u.
        Y = box['Region']['Y']
        W = box['Region']['W']
        H = box['Region']['H']
        
        x = int(X*imw) # in px
        y = int(Y*imh)
        w = int(W*imw)
        h = int(H*imh)
         
        # image with string
        if w > 10 and h > 10 and float(h)/float(w) < 5:
            image_name = images_folder + str(i) + '.png'
            im_string = plane_img[y:y+h, x:x+w, :].copy()
            cv2.imwrite(image_name, im_string) 
        string_boxes[str(i)] = [X, Y, W, H] # in a.u. # ?????? 
        # here we write some comment
    return string_boxes

def json_with_predicted_words(images_folder, words_folder, string_boxes, imw, imh):
    txt_files = [f for f in os.listdir(images_folder) if f.endswith('.txt')]
    #txt_files = txt_files.sort()
    cnt_blank = 0
    cnt_string = 0 # how many string to cut
    word_regions = [] # for new json
    for file in txt_files:
        with open(images_folder + file, 'r') as f:
            key = file[:-9] # key number
            string = f.readline()
            
            if len(string)>1:
                cnt_string += 1
                x = string_boxes[key][0] # in a.u.
                y = string_boxes[key][1]
                w = string_boxes[key][2]
                h = string_boxes[key][3]

                string_list = string.split(' ')
                if len(string_list) > 1:
                    # split into words
                    N = len(string_list)
                    im_string = cv2.imread(images_folder + key + '.png')
                    idx = split_image_string(im_string[:,:,0])
                    if len(idx) > N-1:
                        idx = idx[:N-1] # bug. was :N
                    #print('pict {} has {} words'.format(key, len(idx)))
                    
                    idx = np.sort(idx)
                    idx = np.insert(idx, 0, 0)
                    idx = np.append(idx, im_string.shape[1])
                    
                    kk = np.arange(len(idx)-1)
                    x_word = idx # bug was here: np.delete(idx, -1)
                    w_word = idx[kk+1]-idx[kk]

                    # cutting string - new words
                    im_words = []
                    for j in range(len(idx)-1): # Bug!!! was len(x_word) and loos last word
                        word = string_list[j]
                        xwabs = x + x_word[j]/imw
                        ywabs = y
                        wwabs = w_word[j]/imw
                        hwabs = h
                        
                        im_word = im_string[:, x_word[j]:x_word[j+1], :]
                        if not im_word.size == 0:
                            cv2.imwrite(words_folder + str(key) + '_' + str(j) + '.png', im_word)	
                                                
                        word_regions.append({"Text": word,
                                        "Region": {
                                            "X": xwabs,
                                            "Y": ywabs,
                                            "W": wwabs,
                                            "H": hwabs }
                                        })
                    
                else: 
                    # write original string
                    word_regions.append({"Text": string,
                                        "Region": {
                                            "X": x,
                                            "Y": y,
                                            "W": w,
                                            "H": h }
                                        })
                    cnt_blank += 1
                    # print('Only one word')
                    # print(x, y, w, h)
            # else:
            #     # write blank
            #     word_regions.append({"Text": string,
            #                         "Region": {
            #                                 "X": X,
            #                                 "Y": Y,
            #                                 "W": W,
            #                                 "H": H }
            #                             })
    print('Found {} boxes with no text'.format(cnt_blank))
    print('Splitted {} strings'.format(cnt_string))
    return word_regions


def split_image_string(im_string):
    """ 19/12/19 by N.
        im_string - image with text to split; must be in gray scale
        imw - plane image width
    """
    _, im_string_g = cv2.threshold(im_string, 150, 255, cv2.THRESH_BINARY)
    text_line = (255-im_string_g).sum(axis = 0)
    text_line = text_line / text_line.max()

    text_line[text_line>0.06] = 1 
    text_line[text_line<0.5] = 0

    text_line[:3] = 1 # save bounds from detection white spaces
    text_line[-3:] = 1

    xxx = np.arange(len(text_line))
    z_line = np.zeros(len(text_line))

    spaces = xxx[text_line==0]
    
    b = np.zeros(1) # a kind of histogram
    k = 0 # index of histogram
    j = np.zeros(1, dtype=int) # index where white space starts
    for i in range(len(spaces)-1):
        if spaces[i+1] - spaces[i] == 1:
            b[k] += 1 # new item in a bin  
        else:
            j = np.append(j, spaces[i+1])
            b = np.append(b, 1) # move to new bin 
            k += 1 #  

    b12 = (b/2).astype(int)
    ind_all = j+b12 # indeces moved to half up
    ind_largest = np.argsort(b)[::-1]
    idx = ind_all[ind_largest]
    #idx = ind_all[b>5]
    return idx # in px



def split_json_strings(in_json, out_json, index, width):
    """ 17/12/19 by N.
    """

    with open(in_json, "r") as file:
        text_regions = json.load(file)

    len_original = len(text_regions['TextRegions']) 

    new_boxes = []
    cnt_w = 0
    cnt_s = 0
    for box in text_regions['TextRegions']:
        string = box['Text']
        x = box['Region']['X']
        y = box['Region']['Y']
        w = box['Region']['W']
        h = box['Region']['H']

        string_list = string.split(' ')
        if len(string_list) > 1:
            lpc = w/len(string) # length per char
            w_word = [len(s) * lpc + lpc for s in string_list] 
            w_word = np.asarray(w_word)
            x_word = x + np.cumsum(w_word) - w_word[0]

            N = len(string_list)
            if len(index) > N:
                index = index[:N] # N main white spaces 
            idx = np.sort(index)
            idx = np.insert(idx, 0, 0) # now index starts from the 0 
            idx = np.append(idx, width) # add the last index
            x_word = x + idx
            k = np.arange(len(idx)-1) 
            w_word = idx[k+1]-idx[k]
            
            # split the string!
            #delta = lpc # add some space 
            for i, word in enumerate(string_list):
                x1 = x_word[i]
                y1 = y
                w1 = w_word[i]
                h1 = h
            
                new_box = {}
                new_box['Text'] = word
                new_box['Region'] = {}
                new_box['Region']['X'] = x1
                new_box['Region']['Y'] = y1
                new_box['Region']['W'] = w1
                new_box['Region']['H'] = h1
                
                #print(new_box)
                new_boxes.append(new_box)
                cnt_w += 1 # added one more word
            cnt_s += 1 # splitted one more string 
        else:
            new_boxes.append(box)
                
    print('Added {} words'.format(cnt_w))
    print('Remove {} strings'.format(cnt_s))
    print('The length was {}'.format(len_original))
    print('Now it should be: ')
    print('{} - {} + {} = {}'.format(len_original, cnt_s, cnt_w, len(new_boxes)))
    
    # save new json file
    new_text_regions = {}
    new_text_regions['TextRegions'] = new_boxes
        
    jstr = json.dumps(new_text_regions, indent=2)
    with open(out_json, 'wt') as outfile:
        for s in jstr:
            outfile.write(s)
        print('Wrote to {}'.format(out_json))
    
    return new_boxes



