#Library for combining single symbols into words

import cv2
from os import listdir
from os.path import isfile, join, splitext
import time
import contour_detector
import combine_letters


def main():

    # settings
    visualize_debug = False

    # path configuration
    input_path = 'D:/Users/Snake/WorkAdd/IJ/SymbolRecognition/Data/benchmark_86/good_gt_images/'
    output_path = 'results/'
    image_files = [f for f in listdir(input_path) if isfile(join(input_path, f))]
    #image_files = ['4_2.png', '4_11.png']

    # loop over files, if there are any
    total_time = 0
    for k in range(len(image_files)):

        # read input data
        print(image_files[k])
        img = cv2.imread(join(input_path, image_files[k]))
        height, width, channels = img.shape

        # detection
        plan_start_time = time.time()
        det_start_time = plan_start_time
        threshold = 160
        detections = contour_detector.detect_characters(img,
                                       binary_threshold=threshold,
                                       dots_over_ij=True,
                                       test_output=False)
        det_end_time = time.time()
        print('Detection time ' + str(det_end_time - det_start_time) + ' s')
        print("Number of detections:", len(detections))
        contour_detector.save_detections_to_json_file_test(detections=detections, json_file_name=splitext(image_files[k])[0] + '_letters.json')

        # grouping
        group_start_time = time.time()
        (words, group_img) = combine_letters.combine_letters_simple(detections, img, (0.5, True), \
                                                                False, False, True)
        group_end_time = time.time()
        print('letters number = ', len(detections), 'words number = ', len(words), \
              ' grouping time = ', group_end_time - group_start_time, ' sec')
        combine_letters.write_json_word_reduced(output_path + splitext(image_files[k])[0] + '_strings.json', words, width, height, False, True)
        plan_end_time = time.time()
        total_time += plan_end_time - plan_start_time
        # save words to json

        # visualization part
        if visualize_debug:
            # visualize detections
            contour_detector.draw_detections.check_consistency(detections)
            contour_detector.draw_detections.draw_bounding_boxes(img,
                                                detections,
                                                draw_cnt_rects=True,
                                                draw_ext_rects=True,
                                                draw_pred_rects=True,
                                                draw_only_valid_labels=False)

            contour_detector.draw_detections.draw_bounding_boxes_with_neighbors(img, detections, draw_trio_boxes=False)
            # visualize letters
            img_let = img.copy()
            for letter in detections:
                (x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
                left_n_cnt, n_cnt, right_n_cnt, label = letter
                cv2.rectangle(img_let, (x_ext, y_ext), (x_ext + w_ext, y_ext + h_ext), (0, 0, 255))
            cv2.imwrite(join(output_path, splitext(image_files[k])[0] + '_letters.png'), img_let)
            # visualize neighbors
            img_neigh = img.copy()
            for letter in detections:
                (x_cnt, y_cnt, w_cnt, h_cnt), (x, y, w, h), (x_pred, y_pred, w_pred, h_pred), \
                    left_n_cnt, n_cnt, right_n_cnt, label = letter
                x1 = x
                y1 = y
                x2 = x + w
                y2 = y + h
                if (left_n_cnt >= 0) and (left_n_cnt < len(detections)) and (left_n_cnt != n_cnt):
                    (x_cnt, y_cnt, w_cnt, h_cnt), (lx, ly, lw, lh), (x_pred, y_pred, w_pred, h_pred), \
                        lleft_n_cnt, ln_cnt, lright_n_cnt, llabel = detections[left_n_cnt]
                    x2 = max(x2, lx + lw)
                    y2 = max(y2, ly + lh)
                    x1 = min(x1, lx)
                    y1 = min(y1, ly)
                if (right_n_cnt >= 0) and (right_n_cnt < len(detections)) and (right_n_cnt != n_cnt):
                    (x_cnt, y_cnt, w_cnt, h_cnt), (rx, ry, rw, rh), (x_pred, y_pred, w_pred, h_pred), \
                        rleft_n_cnt, rn_cnt, rright_n_cnt, rlabel = detections[right_n_cnt]
                    x2 = max(x2, rx + rw)
                    y2 = max(y2, ry + rh)
                    x1 = min(x1, rx)
                    y1 = min(y1, ry)
                cv2.rectangle(img_neigh, (x1, y1), (x2, y2), (255, 0, 0))
            cv2.imwrite(join(output_path, splitext(image_files[k])[0] + '_neighbors.png'), img_neigh)
            # visualize words
            img_word = img_let.copy()
            for word in words:
                (wx, wy, ww, wh), _, _, _ = word
                cv2.rectangle(img_word, (wx, wy), (wx + ww, wy + wh), (255, 0, 0))
            cv2.imwrite(join(output_path, splitext(image_files[k])[0] + '_words.png'), img_word)
            # visualize debug
            cv2.imwrite(join(output_path, splitext(image_files[k])[0] + '_debug.png'), group_img)
    print('Total time = ', total_time, ' sec, Average time per plan = ', total_time / len(image_files), ' sec')


if __name__ == '__main__':
    main()
