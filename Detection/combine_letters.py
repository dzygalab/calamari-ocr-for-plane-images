#Library for combining single symbols into words

import cv2
import numpy as np
import json
from os import listdir
from os.path import isfile, join, splitext
import time
import math


# Combinesl etters into words
# letters - letters to be combined
#  ((x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), left_n_cnt, n_cnt, right_n_cnt, label)
# image - image for debug draw
# params:
#  height_ratio - height ratio
#  use_extended_height - use extended bounding box height instead of predicted one
def combine_letters_simple(letters, image, params, \
                     debug_mode = False, enable_timings = False, debug_draw = False):
    (height_ratio, use_extended_height) = params
    start_time = -1
    complete_start_time = -1
    if enable_timings:
        start_time = time.time()
        complete_start_time = start_time

    if debug_mode:
        print('combine_letters_simple(): combining started')
    words = []
    draw_results = False
    if (debug_draw) and (image is not None):
        draw_results = True
        imheight, imwidth, imchannels = image.shape
        if imchannels == 3:
            group_img = image.copy()
        else:
            draw_results = False

    for k in range(len(letters)):
        (x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
            left_n_cnt, n_cnt, right_n_cnt, label = letters[k]
        if k != n_cnt:
            if debug_mode:
                print('combine_letters()_simple: wrong element index.')
            return words

    # marks of contours using
    neighbors = []
    empty_string = 'None'
    for letter in letters:
        (x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
            left_n_cnt, n_cnt, right_n_cnt, label = letter
        if ((use_extended_height) and (h_ext < 0)) or ((not use_extended_height) and (h_cnt < 0)):
            neighbors.append([len(letters), len(letters)])
        else:
            neighbors.append([-1, -1])

    if enable_timings:
        end_time = time.time()
        print('Symbols-to-words-simple (initialization): ', end_time - start_time, ' sec')
        start_time = end_time

    # loop over contours
    for k in range(len(letters)):

        (x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
            left_n_cnt, n_cnt, right_n_cnt, label = letters[k]

        if draw_results:
            if use_extended_height:
                cv2.rectangle(group_img, (x_ext, y_ext), (x_ext + w_ext, y_ext + h_ext), (0, 0, 255))
            else:
                cv2.rectangle(group_img, (x_cnt, y_cnt), (x_cnt + w_cnt, y_cnt + h_cnt), (0, 0, 255))

            if use_extended_height:
                x1 = x_ext
                x2 = x_ext + w_ext
                y1 = y_ext
                y2 = y_ext + h_ext
            else:
                x1 = x_cnt
                x2 = x_cnt + w_cnt
                y1 = y_cnt
                y2 = y_cnt + h_cnt
            if (left_n_cnt >= 0) and (left_n_cnt < len(letters)) and (left_n_cnt != n_cnt):
                (lx_cnt, ly_cnt, lw_cnt, lh_cnt), (lx_ext, ly_ext, lw_ext, lh_ext), (lx_pred, ly_pred, lw_pred, lh_pred), \
                    lleft_n_cnt, ln_cnt, lright_n_cnt, llabel = letters[left_n_cnt]
                if use_extended_height:
                    x1 = min(x1, lx_ext)
                    x2 = max(x2, lx_ext + lw_ext)
                    y1 = min(y1, ly_ext)
                    y2 = max(y2, ly_ext + lh_ext)
                else:
                    x1 = min(x1, lx_cnt)
                    x2 = max(x2, lx_cnt + lw_cnt)
                    y1 = min(y1, ly_cnt)
                    y2 = max(y2, ly_cnt + lh_cnt)
            if (right_n_cnt >= 0) and (right_n_cnt < len(letters)) and (right_n_cnt != n_cnt):
                (rx_cnt, ry_cnt, rw_cnt, rh_cnt), (rx_ext, ry_ext, rw_ext, rh_ext), (rx_pred, ry_pred, rw_pred, rh_pred), \
                    rleft_n_cnt, rn_cnt, rright_n_cnt, rlabel = letters[right_n_cnt]
                if use_extended_height:
                    x1 = min(x1, rx_ext)
                    x2 = max(x2, rx_ext + rw_ext)
                    y1 = min(y1, ry_ext)
                    y2 = max(y2, ry_ext + rh_ext)
                else:
                    x1 = min(x1, rx_cnt)
                    x2 = max(x2, rx_cnt + rw_cnt)
                    y1 = min(y1, ry_cnt)
                    y2 = max(y2, ry_cnt + rh_cnt)
            x1 = max(0, x1 - 1)
            y1 = max(0, y1 - 1)
            x2 = min(imwidth, x2 + 1)
            y2 = min(imheight, y2 + 1)
            cv2.rectangle(group_img, (x1, y1), (x2, y2), (255, 255, 0))

        # search for left neigbor in iterative loop
        k_i = k
        search_left = True
        while search_left:
            search_left = False
            if neighbors[k_i][0] < 0:
                # letter has no left neighbor, search for it iteratively
                (kx_cnt, ky_cnt, kw_cnt, kh_cnt), (kx_ext, ky_ext, kw_ext, kh_ext), (kx_pred, ky_pred, kw_pred, kh_pred), \
                    kleft_n_cnt, kn_cnt, kright_n_cnt, label = letters[k_i]
                if (kleft_n_cnt >= 0) and (kleft_n_cnt < len(letters)):
                    if (use_extended_height):
                        cur_height = kh_ext
                    else:
                        cur_height = kh_cnt
                    # search for left neighbor
                    if neighbors[kleft_n_cnt][1] < 0:
                        # no right neighbor, check it
                        (ix_cnt, iy_cnt, iw_cnt, ih_cnt), (ix_ext, iy_ext, iw_ext, ih_ext), (ix_pred, iy_pred, iw_pred, ih_pred), \
                        ileft_n_cnt, in_cnt, iright_n_cnt, ilabel = letters[kleft_n_cnt]
                        # height ratio
                        if use_extended_height:
                            ratio = cur_height / ih_ext
                        else:
                            ratio = cur_height / ih_cnt
                        if ratio > 1:
                            ratio = 1 / ratio
                        if ratio >= height_ratio:
                            # height ratio is acceptable
                            neighbors[k_i][0] = kleft_n_cnt
                            neighbors[kleft_n_cnt][1] = k_i
                            k_i = kleft_n_cnt
                            search_left = True

        # search for right neigbor in iterative loop
        k_j = k
        search_right = True
        while search_right:
            search_right = False
            if neighbors[k_j][1] < 0:
                # letter has no right neighbor, search for it iteratively
                (kx_cnt, ky_cnt, kw_cnt, kh_cnt), (kx_ext, ky_ext, kw_ext, kh_ext), (kx_pred, ky_pred, kw_pred, kh_pred), \
                    kleft_n_cnt, kn_cnt, kright_n_cnt, label = letters[k_j]
                if (kright_n_cnt >= 0) and (kright_n_cnt < len(letters)):
                    if (use_extended_height):
                        cur_height = kh_ext
                    else:
                        cur_height = kh_cnt
                    # search for right neighbor
                    if neighbors[kright_n_cnt][0] < 0:
                        # no left neighbor, check it
                        (jx_cnt, jy_cnt, jw_cnt, jh_cnt), (jx_ext, jy_ext, jw_ext, jh_ext), (jx_pred, jy_pred, jw_pred, jh_pred), \
                        jleft_n_cnt, jn_cnt, jright_n_cnt, jlabel = letters[kright_n_cnt]
                        # height ratio
                        if use_extended_height:
                            ratio = cur_height / jh_ext
                        else:
                            ratio = cur_height / jh_cnt
                        if ratio > 1:
                            ratio = 1 / ratio
                        if ratio >= height_ratio:
                            # height ratio is acceptable
                            neighbors[k_j][1] = kright_n_cnt
                            neighbors[kright_n_cnt][0] = k_j
                            k_j = kright_n_cnt
                            search_right = True

    if enable_timings:
        end_time = time.time()
        print('Symbols-to-words-simple (neighbors searching): ', end_time - start_time, ' sec')
        start_time = end_time

    # loop over contours
    for k in range(len(letters)):

        if debug_mode:
            print(neighbors[k][0], ' - ', k, ' - ', neighbors[k][1], '    ')
        if (neighbors[k][0] >= len(letters)) or (neighbors[k][1] >= len(letters)):
            # bad contour, exclude it
            if debug_mode:
                print(letters[k][1])
            continue
        if debug_mode:
            if (neighbors[k][0] >= 0) and (neighbors[k][0] < len(letters)):
                print(letters[neighbors[k][0]][1])
            print(letters[k][1])
            if (neighbors[k][1] >= 0) and (neighbors[k][1] < len(letters)):
                print(letters[neighbors[k][1]][1])

        if neighbors[k][0] < 0:
            # no left neighbor, word start

            if neighbors[k][1] < 0:
                # no right member, single symbol
                (x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
                left_n_cnt, n_cnt, right_n_cnt, label = letters[k]
                if use_extended_height:
                    words.append([(x_ext, y_ext, w_ext, h_ext), label, [int(k)], label])
                    if (draw_results):
                        y1 = max(0, y_ext - 3)
                        y2 = min(imheight, y_ext + h_ext + 3)
                        x1 = max(0, x_ext - 3)
                        x2 = min(imwidth, x_ext + w_ext + 3)
                else:
                    words.append([(x_cnt, y_cnt, w_cnt, h_cnt), label, [int(k)], label])
                    if (draw_results):
                        y1 = max(0, y_cnt - 3)
                        y2 = min(imheight, y_cnt + h_cnt + 3)
                        x1 = max(0, x_cnt - 3)
                        x2 = min(imwidth, x_cnt + w_cnt + 3)
                if (draw_results):
                    cv2.rectangle(group_img, (x1, y1), (x2, y2), (255, 0, 0))
            else:
                cur_word = None
                cur_let_list = []
                k_i = k
                x1 = -1
                y1 = -1
                x2 = -1
                y2 = -1
                while True:
                    (x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
                    left_n_cnt, n_cnt, right_n_cnt, label = letters[k_i]
                    if cur_word is None:
                        cur_word = label
                    else:
                        cur_word += label
                    cur_let_list.append(int(k_i))
                    if use_extended_height:
                        if y2 - y1 <= 0:
                            x1 = x_ext
                            x2 = x_ext + w_ext
                            y1 = y_ext
                            y2 = y_ext + h_ext
                        else:
                            x1 = min(x1, x_ext)
                            x2 = max(x2, x_ext + w_ext)
                            y1 = min(y1, y_ext)
                            y2 = max(y2, y_ext + h_ext)
                    else:
                        if y2 - y1 <= 0:
                            x1 = x_cnt
                            x2 = x_cnt + w_cnt
                            y1 = y_cnt
                            y2 = y_cnt + h_cnt
                        else:
                            y1 = min(y1, y_cnt)
                            y2 = max(y2, y_cnt + h_cnt)
                            x1 = min(x1, x_cnt)
                            x2 = max(x2, x_cnt + w_cnt)
                    k_i = neighbors[k_i][1]
                    if (k_i < 0) or (k_i >= len(letters)) or (k_i == k):
                        break
                words.append([(x1, y1, x2 - x1, y2 - y1), cur_word, cur_let_list, cur_word])
                if (draw_results):
                    x1 = max(0, x1 - 2)
                    y1 = max(0, y1 - 2)
                    x2 = min(imwidth, x2 + 2)
                    y2 = min(imheight, y2 + 2)
                    cv2.rectangle(group_img, (x1, y1), (x2, y2), (0, 255, 0))

    if enable_timings:
        end_time = time.time()
        print('Symbols-to-words-simple (words): ', end_time - start_time, ' sec')
        print('Symbols-to-words-simple (complete): ', end_time - complete_start_time, ' sec')

    if debug_mode:
        print('combine_letters(): combining finished')
    if draw_results:
        return (words, group_img)
    else:
        return (words, None)


# reading json file with letters in reduced form
# json_name - json file name
# width - image width (json in reduced form includes only relative bounding rects)
# height - image height
def read_json_letter_reduced(json_name, width, height, debug_mode = False):
    letters = []
    if debug_mode:
        print('read_json_letter_reduced(): reading started')
    with open(json_name) as json_file:
        data = json.load(json_file)
        for tr in data['TextRegions']:
            l = tr['Text']
            rx = tr['Region']['X'] * width
            ry = tr['Region']['Y'] * height
            rw = tr['Region']['W'] * width
            rh = tr['Region']['H'] * height
            letters.append([(rx, ry, rw, rh), l])
            if debug_mode:
                print('l = ', l, ' r = (', rx, ', ', ry, ', ', rw, ', ', rh, ')')
    if debug_mode:
        print('read_json_letter_reduced(): reading finished')
    return letters


# reading json file with letters in complete form
# json_name - json file name
def read_json_letter_complete(json_name, debug_mode = False):
    letters = []
    if debug_mode:
        print('read_json_letter_complete(): reading started')
    #empty_string = 'None'
    with open(json_name) as json_file:
        data = json.load(json_file)
        for tr in data['TextRegions']:
            label = tr['Text']
            #if (len(label) <= 0) or (label == empty_string):
            #    label = None
            x_cnt = tr['Region']['x_cnt']
            y_cnt = tr['Region']['y_cnt']
            w_cnt = tr['Region']['w_cnt']
            h_cnt = tr['Region']['h_cnt']
            x_ext = tr['Region']['x_ext']
            y_ext = tr['Region']['y_ext']
            w_ext = tr['Region']['w_ext']
            h_ext = tr['Region']['h_ext']
            x_pred = tr['Region']['x_pred']
            y_pred = tr['Region']['y_pred']
            w_pred = tr['Region']['w_pred']
            h_pred = tr['Region']['h_pred']
            left_n_cnt = tr['Region']['left_n_cnt']
            n_cnt = tr['Region']['n_cnt']
            right_n_cnt = tr['Region']['right_n_cnt']
            letters.append([(x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
                           left_n_cnt, n_cnt, right_n_cnt, label])
            if debug_mode:
                print('l = ', label, ' cnt = (', x_cnt, ', ', y_cnt, ', ', w_cnt, ', ', h_cnt, '), ext = (', \
                      x_ext, ', ', y_ext, ', ', w_ext, ', ', h_ext, '), pred = (', x_pred, ', ', y_pred, ', ', \
                      w_pred, ', ', h_pred, ')')
    if debug_mode:
        print('read_json_letter_complete(): reading finished')
    return letters


# reading json file with words
# json_name - json file name
# words - words to be written
# width - image width
# height - image height
def write_json_word_reduced(json_name, words, width, height, debug_mode=False, write_json=False, write_letters_list = False):
    if debug_mode:
        print('write_json_words_reduced(): writing started')
    write_absolute = False
    write_basic = False
    out_data = {}
    out_data['TextRegions'] = []
    for k in range(len(words)):
        (wx, wy, ww, wh), word, let_list, basic_word = words[k]
        r = {}
        r['X'] = wx / width
        r['Y'] = wy / height
        r['W'] = ww / width
        r['H'] = wh / height
        out_data['TextRegions'].append({'Text': word, 'Region': r})
        if write_absolute:
            ar = {}
            ar['X'] = wx
            ar['Y'] = wy
            ar['W'] = ww
            ar['H'] = wh
            out_data['TextRegions'][k]['AbsoluteRegion'] = let_list
            if debug_mode:
                print('word = ', word, ' r = (', r['X'], ', ', r['Y'], ', ', r['W'], ', ', r['H'], '), ar = (', \
                      ar['X'], ', ', ar['Y'], ', ', ar['W'], ', ', ar['H'], ')')
        else:
            if debug_mode:
                print('word = ', word, ' r = (', r['X'], ', ', r['Y'], ', ', r['W'], ', ', r['H'], ')')
        if write_letters_list:
            out_data['TextRegions'][k]['LettersList'] = let_list
        if write_basic:
            out_data['TextRegions'][k]['BasicWord'] = basic_word

    if write_json:
        jstr = json.dumps(out_data, indent=2)
        with open(json_name, 'w') as out_file:
            for s in jstr:
                out_file.write(s)
        if debug_mode:
            print('write_json_words_reduced(): writing finished')

    return out_data

# writing text to a json file
# json_name - json file name
# textstrings - text lines with no bounding rects
# roi - region of interest containing the text
# width - image width
# height - image height
def write_json_text(json_name, textstrings, roi, width, height, debug_mode=False, write_json=False):
    if debug_mode:
        print('write_json_text(): writing started')
    write_absolute = False
    out_data = {}
    out_data['TextRegions'] = []
    cstring = ''
    for ts in textstrings:
        cstring += ts + '\n'
    (rx, ry, rw, rh) = roi
    r = {}
    r['X'] = rx / width
    r['Y'] = ry / height
    r['W'] = rw / width
    r['H'] = rh / height
    if write_absolute:
        ar = {}
        ar['X'] = rx
        ar['Y'] = ry
        ar['W'] = rw
        ar['H'] = rh
        out_data['TextRegions'].append({'Text': cstring, 'Region': r, 'AbsoluteRegion': ar})
        if debug_mode:
            print('word = ', cstring, ' r = (', r['X'], ', ', r['Y'], ', ', r['W'], ', ', r['H'], '), ar = (', \
                  ar['X'], ', ', ar['Y'], ', ', ar['W'], ', ', ar['H'], ')')
    else:
        out_data['TextRegions'].append({'Text': cstring, 'Region': r})
        if debug_mode:
            print('word = ', cstring, ' r = (', r['X'], ', ', r['Y'], ', ', r['W'], ', ', r['H'], ')')

    if write_json:
        jstr = json.dumps(out_data, indent=2)
        with open(json_name, 'w') as out_file:
            for s in jstr:
                out_file.write(s)
        if debug_mode:
            print('write_json_text(): writing finished')

    return out_data

# writing text to a text file
# json_name - text file name
# textstrings - text lines with no bounding rects
def write_text_text(text_name, textstrings, debug_mode = False):
    if debug_mode:
        print('write_text_text(): writing started')
        print('Strings number = ', len(textstrings))
    with open(text_name, 'w') as out_text:
        for ts in textstrings:
            out_text.write(ts + '\n')
            if (debug_mode):
                print(ts)
    if debug_mode:
        print('write_text_text(): writing finished')


def main():

    # settings
    extend_rects = True
    save_words = True
    visualize_letters = True
    visualize_neighbors = True
    visualize_words = True
    visualize_debug = True

    # path configuration
    input_path = 'D:/Users/Snake/WorkAdd/IJ/SymbolRecognition/Data/benchmark_86/'
    image_dir = join(input_path, 'good_gt_images/')
    json_dir = join(input_path, 'json_84_26_11_chars/')
    output_path = 'results/combine/'
    #image_files = [f for f in listdir(image_dir) if isfile(join(image_dir, f))]
    #json_files = [f for f in listdir(json_dir) if isfile(join(json_dir, f))]
    image_files = ['1_3.png']
    json_files = ['1_3.json']

    # loop over files, if there are any
    total_word_time = 0
    if len(image_files) == len(json_files):
        for k in range(len(json_files)):

            # read input data
            print(json_files[k], ' - ', image_files[k])
            img = cv2.imread(join(image_dir, image_files[k]))
            height, width, channels = img.shape
            #letters = read_json_letter_reduced(join(json_dir, json_files[k]), width, height)
            letters = read_json_letter_complete(join(json_dir, json_files[k]))

            # group letters to words
            plan_start_time = time.time()
            (words, group_img) = combine_letters_simple(letters, img, (0.5, extend_rects), \
                                                                False, False, visualize_debug)
            plan_end_time = time.time()
            print('letters number = ', len(letters), 'words number = ', len(words), \
                  ' time = ', plan_end_time - plan_start_time, ' sec')
            total_word_time += plan_end_time - plan_start_time
            # save words to json
            if save_words:
                write_json_word_reduced(join(output_path, json_files[k]), words, width, height)

            # visualization part
            empty_string = 'None'
            # visualize letters
            if visualize_letters:
                img_let = img.copy()
                for letter in letters:
                    (x_cnt, y_cnt, w_cnt, h_cnt), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
                        left_n_cnt, n_cnt, right_n_cnt, label = letter
                    if extend_rects:
                        cv2.rectangle(img_let, (x_ext, y_ext), (x_ext + w_ext, y_ext + h_ext), (0, 0, 255))
                    else:
                        cv2.rectangle(img_let, (x_cnt, y_cnt), (x_cnt + w_cnt, y_cnt + h_cnt), (0, 0, 255))
                cv2.imwrite(join(output_path, splitext(json_files[k])[0] + '_letters.png'), img_let)
            #visualize neighbors
            if visualize_neighbors:
                img_neigh = img.copy()
                for letter in letters:
                    if extend_rects:
                        (x_cnt, y_cnt, w_cnt, h_cnt), (x, y, w, h), (x_pred, y_pred, w_pred, h_pred), \
                            left_n_cnt, n_cnt, right_n_cnt, label = letter
                    else:
                        (x, y, w, h), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
                            left_n_cnt, n_cnt, right_n_cnt, label = letter
                    x1 = x
                    y1 = y
                    x2 = x + w
                    y2 = y + h
                    if (left_n_cnt >= 0) and (left_n_cnt < len(letters)) and (left_n_cnt != n_cnt):
                        if extend_rects:
                            (x_cnt, y_cnt, w_cnt, h_cnt), (lx, ly, lw, lh), (x_pred, y_pred, w_pred, h_pred), \
                                lleft_n_cnt, ln_cnt, lright_n_cnt, llabel = letters[left_n_cnt]
                        else:
                            (lx, ly, lw, lh), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_red, w_pred, h_pred), \
                                lleft_n_cnt, ln_cnt, lright_n_cnt, llabel = letters[left_n_cnt]
                        x2 = max(x2, lx + lw)
                        y2 = max(y2, ly + lh)
                        x1 = min(x1, lx)
                        y1 = min(y1, ly)
                    if (right_n_cnt >= 0) and (right_n_cnt < len(letters)) and (right_n_cnt != n_cnt):
                        if extend_rects:
                            (x_cnt, y_cnt, w_cnt, h_cnt), (rx, ry, rw, rh), (x_pred, y_pred, w_pred, h_pred), \
                                rleft_n_cnt, rn_cnt, rright_n_cnt, rlabel = letters[right_n_cnt]
                        else:
                            (rx, ry, rw, rh), (x_ext, y_ext, w_ext, h_ext), (x_pred, y_pred, w_pred, h_pred), \
                                rleft_n_cnt, rn_cnt, rright_n_cnt, rlabel = letters[right_n_cnt]
                        x2 = max(x2, rx + rw)
                        y2 = max(y2, ry + rh)
                        x1 = min(x1, rx)
                        y1 = min(y1, ry)
                    cv2.rectangle(img_neigh, (x1, y1), (x2, y2), (255, 0, 0))
                cv2.imwrite(join(output_path, splitext(json_files[k])[0] + '_neighbors.png'), img_neigh)
            # visualize words
            if visualize_words:
                if visualize_letters:
                    img_word = img_let.copy()
                else:
                    img_word = img.copy()
                for word in words:
                    (wx, wy, ww, wh), _, _, _ = word
                    cv2.rectangle(img_word, (wx, wy), (wx + ww, wy + wh), (255, 0, 0))
                cv2.imwrite(join(output_path, splitext(json_files[k])[0] + '_words.png'), img_word)
            # visualize debug
            if visualize_debug:
                cv2.imwrite(join(output_path, splitext(json_files[k])[0] + '_debug.png'), group_img)

        print('Total words time = ', total_word_time, ' sec, Average time per plan = ', total_word_time / len(image_files), ' sec')
    else:
        print('Number of images is not equal to the number of json files')


if __name__ == '__main__':
    main()
