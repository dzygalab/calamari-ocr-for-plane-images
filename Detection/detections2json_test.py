import json
from settings import ProjectSettings


def save_detections_to_json_file_test(detections,
                                      json_file_name="ocr_test_results.json"):
    text_regions = []

    for detection in detections:
        ((x_cnt, y_cnt, w_cnt, h_cnt),
         (x_ext, y_ext, w_ext, h_ext),
         (x_pred, y_pred, w_pred, h_pred),
         n_det_left, n_det_center, n_det_right,
         char_label) = detection

        if char_label is None:
            char_label = "None"

        text_regions.append({"Text": char_label,
                             "Region": {
                                 "x_cnt": int(x_cnt),
                                 "y_cnt": int(y_cnt),
                                 "w_cnt": int(w_cnt),
                                 "h_cnt": int(h_cnt),

                                 "x_ext": int(x_ext),
                                 "y_ext": int(y_ext),
                                 "w_ext": int(w_ext),
                                 "h_ext": int(h_ext),

                                 "x_pred": int(x_pred),
                                 "y_pred": int(y_pred),
                                 "w_pred": int(w_pred),
                                 "h_pred": int(h_pred),

                                 "left_n_cnt": int(n_det_left),
                                 "n_cnt": int(n_det_center),
                                 "right_n_cnt": int(n_det_right)
                             }
                             })

    results_for_json = {"TextRegions": text_regions}

    output_file = ProjectSettings.root_path + "results/" + json_file_name

    jstr = json.dumps(results_for_json, indent=2)

    with open(output_file, 'wt') as outfile:
        for s in jstr:
            outfile.write(s)
        # print('Successfully wrote results to file: ' + output_file)
