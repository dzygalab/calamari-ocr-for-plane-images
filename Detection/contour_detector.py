import cv2
import numpy as np
import time

import draw_detections
from settings import ProjectSettings

from detections2json_test import save_detections_to_json_file_test


# def get_children_indexes(hierarchy, parents_indexes):
#     children_indexes = []
#     for cnt_idx, cnt_record in enumerate(hierarchy[0]):
#         if cnt_record[3] in parents_indexes:
#             children_indexes.append(cnt_idx)
#     return children_indexes


def get_children_indexes_fast(hierarchy, parents_indexes):
    first_children = []
    for parent_idx in parents_indexes:
        first_child = hierarchy[0][parent_idx][2]
        if first_child != -1:
            if first_child not in first_children:
                first_children.append(first_child)

    children_indexes = []
    for first_child in first_children:
        children_indexes.append(first_child)
        next_child = hierarchy[0][first_child][0]
        while next_child != -1:
            children_indexes.append(next_child)
            next_child = hierarchy[0][next_child][0]

    return children_indexes


def is_it_a_dot_contour(hierarchy, contour_attributes):
    cnt_idx, cnt_area, cnt_rect, rect_area, rect_aspect_ratio, extent, cnt_perimeter_ratio = contour_attributes

    children_indexes = get_children_indexes_fast(hierarchy, parents_indexes=[cnt_idx])
    if children_indexes:
        return False

    min_char_rect_area = 36
    if rect_area <= min_char_rect_area:
        return True

    rect_aspect_ratio_th = 0.65   # 0.60    # 0.625 = solid hyphen in sheet number
    if rect_aspect_ratio < rect_aspect_ratio_th:
        return False

    cnt_perimeter_ratio_th = 0.90
    if cnt_perimeter_ratio > cnt_perimeter_ratio_th:
        return False

    extent_th = 0.55    # 0.625
    if extent >= extent_th:
        return True
    else:
        return False


def test_grandchildren(hierarchy, min_char_rect_area,
                       normal_contour_n_index_to_test,
                       normal_contours_attributes,
                       bounding_contours_n_indexes,
                       internal_dot_contours_n_indexes):

    internal_dot_contours_n_indexes_local = []

    #  Normal contour to test
    normal_raw_cnt_idx_to_test = normal_contours_attributes[normal_contour_n_index_to_test][0]
    normal_cnt_rect_area_to_test = normal_contours_attributes[normal_contour_n_index_to_test][3]

    # Find positive grandchild contours
    children_raw_cnt_indexes = get_children_indexes_fast(hierarchy, parents_indexes=[normal_raw_cnt_idx_to_test])
    grandchildren_raw_cnt_indexes = get_children_indexes_fast(hierarchy, parents_indexes=children_raw_cnt_indexes)

    # Test all grandchildren
    for grandchild_raw_cnt_idx in grandchildren_raw_cnt_indexes:
        # Search through all normal contours
        for n_index, attributes in enumerate(normal_contours_attributes):
            normal_raw_cnt_idx = attributes[0]
            normal_cnt_rect_area = attributes[3]

            if grandchild_raw_cnt_idx == normal_raw_cnt_idx:

                if is_it_a_dot_contour(hierarchy, contour_attributes=attributes):
                    # We have an internal dot contour!
                    internal_dot_contours_n_indexes_local.append(n_index)
                elif normal_cnt_rect_area >= min_char_rect_area:
                    # We have an internal character!
                    # It means that the contour under test is a bounding contour.
                    bounding_contours_n_indexes.append(normal_contour_n_index_to_test)
                    # Discard all collected internal_dot_contours_n_indexes_local!!!
                    return

    # If we are here then the normal_contour_n_index_to_test is NOT a bounding contour.
    # And we must save internal_dot_contours_n_indexes_LOCAL (if any) to internal_dot_contours_n_indexes!!!
    if internal_dot_contours_n_indexes_local:
        internal_dot_contours_n_indexes.extend(internal_dot_contours_n_indexes_local)


def detect_contours_of_characters(plan_img_bgr, binary_threshold=160, test_output=False):
    # *****************
    # Step 1
    # Find raw contours
    # *****************

    plan_img_g = cv2.cvtColor(plan_img_bgr, cv2.COLOR_BGR2GRAY)
    if test_output:
        cv2.imwrite(ProjectSettings.root_path + "results/01_plan_img_g.png", plan_img_g)

    threshed_img_g = cv2.threshold(plan_img_g, binary_threshold, 255, cv2.THRESH_BINARY)[1]
    if test_output:
        cv2.imwrite(ProjectSettings.root_path + "results/02_threshed_img_g.png", threshed_img_g)

    opencv_ver = int(cv2.__version__[0])
    if opencv_ver >= 4:
        raw_contours, hierarchy = cv2.findContours(threshed_img_g, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    else:
        im2, raw_contours, hierarchy = cv2.findContours(threshed_img_g, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if test_output:
        raw_contours_img_bgr = plan_img_bgr.copy()
        cv2.drawContours(raw_contours_img_bgr, raw_contours, -1, (255, 0, 255), 1)
        cv2.imwrite(ProjectSettings.root_path + "results/03_raw_contours_img_bgr.png", raw_contours_img_bgr)
        print("Number of raw contours:", len(raw_contours))

    # print("type(contours):", type(contours))    # type(contours): <class 'list'>
    # print("type(contours[0]):", type(contours[0]))    # type(contours[0]): <class 'numpy.ndarray'>
    # print("contours[0].shape:", contours[0].shape)    # contours[0].shape: (4, 1, 2)
    # Number 4 is the number of points that being connected by lines to form the contour (4 points mean quadrangle here)
    # Number 2 is for coordinates of points [x, y]

    # print("type(hierarchy):", type(hierarchy))    # type(hierarchy): <class 'numpy.ndarray'>
    # print("hierarchy.shape:", hierarchy.shape)    # hierarchy.shape: (1, 33, 4)
    # 33 rows are the number of contours. The rows come in the same order as the contours in the contours list
    # 4 columns are for [idx_next, idx_previous, idx_1st_child, idx_parent]
    # idx_next and idx_previous are the next and previous contours at this level of hierarchy tree
    # idx_1st_child is the 1st child in the next (deeper) level of hierarchy tree
    # idx_parent is the parent contour in the previous (upper) level of hierarchy tree
    # idx = -1 means that there is no next, or previous, or child, or parent contours

    # print("hierarchy:")
    # print(hierarchy[0])

    # ************************************************************
    # Step 2
    # Keep all positive contours and discard all negative contours
    # ************************************************************

    raw_negative_contours_to_draw = []    # To be discarded
    raw_positive_contours_to_draw = []    # To keep!
    raw_positive_contour_indexes = []
    raw_positive_contour_areas = []

    for raw_cnt_idx, raw_cnt in enumerate(raw_contours):
        # Contour area: positive or negative
        cnt_area = cv2.contourArea(raw_cnt, oriented=True)
        if cnt_area > 0:
            raw_positive_contours_to_draw.append(raw_cnt)
            raw_positive_contour_indexes.append(raw_cnt_idx)
            raw_positive_contour_areas.append(cnt_area)
        else:
            raw_negative_contours_to_draw.append(raw_cnt)

    if test_output:
        pn_contours_img_bgr = plan_img_bgr.copy()
        cv2.drawContours(pn_contours_img_bgr, raw_negative_contours_to_draw, -1, (0, 255, 0), 1)
        cv2.drawContours(pn_contours_img_bgr, raw_positive_contours_to_draw, -1, (255, 0, 255), 1)
        cv2.imwrite(ProjectSettings.root_path + "results/04_raw_pn_contours_img_bgr.png", pn_contours_img_bgr)

    # ***************************************************
    # Step 3
    # Classify positive contours by size and aspect ratio
    # ***************************************************

    min_dot_rect_area = 5
    tiny_contours_to_draw = []    # To be discarded
    tiny_contours_rect_areas = []

    max_char_rect_area = 1000000
    big_contours_to_draw = []    # To be discarded
    big_contours_rect_areas = []

    min_char_rect_ratio = 0.05
    thin_contours_to_draw = []    # To be discarded
    thin_contours_rect_ratios = []

    min_char_rect_area = 24

    # Normal contours include both:
    # 1) small dots:  min_dot_rect_area < rect_areas < min_char_rect_area
    # 2) characters: min_char_rect_area <= rect_areas <= max_char_rect_area

    normal_contours_to_draw = []
    normal_contours_attributes = []    # List of attribute lists:
    # [cnt_idx, cnt_area, cnt_rect, cnt_rect_area, cnt_rect_ratio, cnt_extent, cnt_perimeter_ratio]

    min_extent = 10000

    for raw_cnt_idx, cnt_area in zip(raw_positive_contour_indexes, raw_positive_contour_areas):
        # Positive contour
        cnt = raw_contours[raw_cnt_idx]
        # Bounding rect
        x, y, w, h = cv2.boundingRect(cnt)
        # print(cnt_idx, "bounding_rect:", (x, y, w, h))
        # Bounding rect area
        rect_area = w * h
        # print(cnt_idx, "cnt_area:", cnt_area)
        # print(cnt_idx, "rect_area:", rect_area)
        # Bounding rect aspect ratio
        rect_aspect_ratio = w / h
        # print(cnt_idx, "min_rect_aspect_ratio, w/h:", min(w, h) / max(w, h))
        # print(cnt_idx, "max_rect_aspect_ratio, w/h:", max(w, h) / min(w, h))
        # Contour extent
        extent = abs(cnt_area) / rect_area
        # print(cnt_idx, "extent:", extent)

        # Contour perimeter
        cnt_perimeter_t = cv2.arcLength(cnt, closed=True)
        # cnt_perimeter_f = cv2.arcLength(cnt, closed=False)    # Strange thing...

        # print(cnt_idx, "cnt_perimeter_t:", cnt_perimeter_t)
        # print(cnt_idx, "rect_perimeter:", 2*(w+h))
        cnt_perimeter_ratio = cnt_perimeter_t / (2*(w+h))
        # print(cnt_idx, "perimeter_ratio:", cnt_perimeter_ratio)

        # print(cnt_idx, "cnt_perimeter_f:", cnt_perimeter_f)

        mean_gray = np.mean(plan_img_g[y: y+h, x: x+w])

        if mean_gray >= 220:
            # Too much of white
            pass

        elif extent < 0.2:    # 0.1:
            # Not a character!
            # print("Skip by extent < 0.2")
            pass

        elif rect_area < min_dot_rect_area:
            tiny_contours_to_draw.append(cnt)
            tiny_contours_rect_areas.append(rect_area)

        elif rect_area > max_char_rect_area:
            big_contours_to_draw.append(cnt)
            big_contours_rect_areas.append(rect_area)

        elif rect_aspect_ratio < min_char_rect_ratio or rect_aspect_ratio > 1.0 / min_char_rect_ratio:
            thin_contours_to_draw.append(cnt)
            thin_contours_rect_ratios.append(rect_aspect_ratio)

        else:
            # min_dot_rect_area <= rect_area <= max_char_rect_area
            normal_contours_to_draw.append(cnt)
            normal_contours_attributes.append([raw_cnt_idx, cnt_area, [x, y, w, h], rect_area, rect_aspect_ratio, extent, cnt_perimeter_ratio])

            if min_extent > extent:
                min_extent = extent

    if test_output:
        print("min_extent:", min_extent)

    if test_output:
        classified_contours_img_bgr = plan_img_bgr.copy()
        cv2.drawContours(classified_contours_img_bgr, big_contours_to_draw, -1, (255, 0, 0), 1)
        cv2.drawContours(classified_contours_img_bgr, thin_contours_to_draw, -1, (0, 128, 255), 1)
        cv2.drawContours(classified_contours_img_bgr, tiny_contours_to_draw, -1, (0, 0, 255), 1)
        cv2.drawContours(classified_contours_img_bgr, normal_contours_to_draw, -1, (255, 0, 255), 1)
        cv2.imwrite(ProjectSettings.root_path + "results/05_classified_contours_img_bgr.png", classified_contours_img_bgr)

        normal_contours_rects_img_bgr = plan_img_bgr.copy()
        for attributes in normal_contours_attributes:
            x, y, w, h = attributes[2]
            cv2.rectangle(img=normal_contours_rects_img_bgr,
                          pt1=(x, y), pt2=(x + w - 1, y + h - 1),
                          color=(255, 0, 255), thickness=1)
        cv2.imwrite(ProjectSettings.root_path + "results/06_normal_contours_rects_img_bgr.png", normal_contours_rects_img_bgr)

        for attributes in normal_contours_attributes:
            if is_it_a_dot_contour(hierarchy, attributes):
                x, y, w, h = attributes[2]
                cv2.rectangle(img=normal_contours_rects_img_bgr,
                              pt1=(x, y), pt2=(x + w - 1, y + h - 1),
                              color=(255, 0, 0), thickness=1)
        cv2.imwrite(ProjectSettings.root_path + "results/07_normal_dot_contours_rects_img_bgr.png", normal_contours_rects_img_bgr)

    if test_output:
        if tiny_contours_rect_areas:
            print("np.min(tiny_contours_rect_areas):", np.min(tiny_contours_rect_areas))
            print("np.median(tiny_contours_rect_areas):", np.median(tiny_contours_rect_areas))
            print("np.max(tiny_contours_rect_areas):", np.max(tiny_contours_rect_areas))
        else:
            print("No tiny_contours!")

        if big_contours_rect_areas:
            print("np.min(big_contours_rect_area):", np.min(big_contours_rect_areas))
            print("np.median(big_contours_rect_area):", np.median(big_contours_rect_areas))
            print("np.max(big_contours_rect_area):", np.max(big_contours_rect_areas))
        else:
            print("No big_contours!")

        if thin_contours_rect_ratios:
            print("np.min(thin_contours_rect_ratios):", np.min(thin_contours_rect_ratios))
            print("np.median(thin_contours_rect_ratios):", np.median(thin_contours_rect_ratios))
            print("np.max(thin_contours_rect_ratios):", np.max(thin_contours_rect_ratios))
        else:
            print("No thin_contours!")

    # ****************************************************************************************************************
    # Step 4
    # Test grandchildren
    # 1) Find and discard those normal_contours that have a char_contours (except dot contours) as a grandchild.
    # 2) Find and remove internal dot contours from normal_contours ("dot in zero with dot").
    # ****************************************************************************************************************

    bounding_contours_to_draw = []    # To be discarded
    bounding_contours_n_indexes = []    # These are indexes of the normal_contours list, NOT the cnt_idx!!!

    internal_dot_contours_to_draw = []    # To be discarded
    internal_dot_contours_n_indexes = []    # These are indexes of the normal_contours list, NOT the cnt_idx!!!

    char_normal_contours_to_draw = []
    char_normal_contours_attributes = []

    for normal_contour_n_index_to_test in range(len(normal_contours_attributes)):
        test_grandchildren(hierarchy, min_char_rect_area,
                           normal_contour_n_index_to_test,
                           normal_contours_attributes,
                           bounding_contours_n_indexes,
                           internal_dot_contours_n_indexes)

    if test_output:
        print("Normal contours:", len(normal_contours_attributes))
        print("Bounding contours:", len(bounding_contours_n_indexes))
        print("Internal dot contours:", len(internal_dot_contours_n_indexes))

    for normal_contour_n_index in range(len(normal_contours_attributes)):
        if normal_contour_n_index in bounding_contours_n_indexes:
            bounding_contours_to_draw.append(normal_contours_to_draw[normal_contour_n_index])

        elif normal_contour_n_index in internal_dot_contours_n_indexes:
            internal_dot_contours_to_draw.append(normal_contours_to_draw[normal_contour_n_index])

        else:
            char_normal_contours_to_draw.append(normal_contours_to_draw[normal_contour_n_index])
            char_normal_contours_attributes.append(normal_contours_attributes[normal_contour_n_index])

    if test_output:
        print("char_normal_contours_to_draw:", len(char_normal_contours_to_draw))
        print("bounding_contours_to_draw:", len(bounding_contours_to_draw))
        print("internal_dot_contours_to_draw:", len(internal_dot_contours_to_draw))

    if test_output:
        bounding_internal_char_contours_img_bgr = plan_img_bgr.copy()
        cv2.drawContours(bounding_internal_char_contours_img_bgr, bounding_contours_to_draw, -1, (0, 255, 0), 1)
        cv2.drawContours(bounding_internal_char_contours_img_bgr, internal_dot_contours_to_draw, -1, (255, 0, 0), 1)
        cv2.drawContours(bounding_internal_char_contours_img_bgr, char_normal_contours_to_draw, -1, (255, 0, 255), 1)
        cv2.imwrite(ProjectSettings.root_path + "results/08_bounding_internal_dots_characters_contours_img_bgr.png", bounding_internal_char_contours_img_bgr)

        char_normal_contours_rects_img_bgr = plan_img_bgr.copy()
        for attributes in char_normal_contours_attributes:
            x, y, w, h = attributes[2]
            cv2.rectangle(img=char_normal_contours_rects_img_bgr,
                          pt1=(x, y), pt2=(x + w - 1, y + h - 1),
                          color=(255, 0, 255), thickness=1)
        cv2.imwrite(ProjectSettings.root_path + "results/09_char_normal_contours_rects_img_bgr.png", char_normal_contours_rects_img_bgr)

    return hierarchy, char_normal_contours_to_draw, char_normal_contours_attributes


def create_contour_map(plan_img_w, plan_ing_h, contours, contours_attributes, test_output):
    contour_map = np.zeros(shape=(plan_ing_h, plan_img_w), dtype=np.uint32)

    for n_cnt in range(len(contours)):
        x, y, w, h = contours_attributes[n_cnt][2]
        contour_map[y: y + h, x: x + w] = n_cnt + 1

    if test_output:
        cv2.imwrite(ProjectSettings.root_path + "results/10_contour_map.png", 255 - 255 * (contour_map / np.max(contour_map)))

    return contour_map


# def create_contour_map_with_contours(plan_img_w, plan_ing_h, contours, contours_attributes, test_output):
#     contour_map = np.zeros(shape=(plan_ing_h, plan_img_w), dtype=np.uint32)
#     contour_map_test = np.zeros(shape=(plan_ing_h, plan_img_w), dtype=np.uint8)
#     for n_cnt in range(len(contours)):
#         x, y, w, h = contours_attributes[n_cnt][2]
#
#         # Version 1
#         mask = np.zeros(shape=(h, w), dtype=np.uint8)
#         cv2.drawContours(mask, [contours[n_cnt]], -1, 1, -1, offset=(-x, -y))
#         mask *= contour_map[y: y + h, x: x + w] == 0
#         # Version 2
#         # contour_map[y: y + h, x: x + w] = 0
#         # contour_map_test[y: y + h, x: x + w] = 0
#         # mask = np.ones(shape=(h, w), dtype=np.uint8)
#
#         contour_map[y: y + h, x: x + w] += mask[:, :].astype(np.uint32) * (n_cnt+1)
#         contour_map_test[y: y + h, x: x + w] += mask[:, :] * 255
#
#     if test_output:
#         cv2.imwrite(ProjectSettings.root_path + "results/10_contour_map_test.png", 255 - contour_map_test)
#         cv2.imwrite(ProjectSettings.root_path + "results/10_contour_map.png", 255 - 255 * (contour_map / np.max(contour_map)))
#
#     return contour_map


# def unique_indexes(contour_map_patch):
#     indexes = []
#     h, w = contour_map_patch.shape
#     for x in range(w):
#         for y in range(h):
#             index = contour_map_patch[y, x]
#             if index > 0:
#                 if not (index in indexes):
#                     indexes.append(index)
#     return indexes


def attach_dots_over_ij(plan_img_bgr, hierarchy, contour_map, contours, contours_attributes, test_output=False):
    dots_over_ij_contours_to_draw = []
    dots_over_ij_contours_n_indexes = []
    plan_h, plan_w = contour_map.shape[:2]
    for n_cnt, attributes in enumerate(contours_attributes):
        if is_it_a_dot_contour(hierarchy, attributes):
            x_dot, y_dot, w_dot, h_dot = attributes[2]
            x1 = x_dot + w_dot//2 - 1
            x2 = x_dot + w_dot//2 + 1 + 1
            y1 = y_dot + h_dot
            # y2 = min(plan_h, y_dot + h_dot + min(w_dot, h_dot))   # old
            y2 = min(plan_h, y_dot + h_dot + min(1, w_dot//2, h_dot//2))   # +++
            n_indexes = list(np.unique(contour_map[y1: y2, x1: x2]))
            if n_indexes:
                for n_index in n_indexes:
                    if (n_index > 0) and ((n_index-1) != n_cnt):
                        x_host, y_host, w_host, h_host = contours_attributes[n_index-1][2]

                        x1_host_new = min(x_host, x_dot)
                        y1_host_new = min(y_host, y_dot)
                        x2_host_new = max(x_host + w_host - 1, x_dot + w_dot - 1)
                        y2_host_new = max(y_host + h_host - 1, y_dot + h_dot - 1)
                        w_host_new = x2_host_new - x1_host_new + 1
                        h_host_new = y2_host_new - y1_host_new + 1

                        contours_attributes[n_index-1][2][0] = x1_host_new
                        contours_attributes[n_index-1][2][1] = y1_host_new
                        contours_attributes[n_index-1][2][2] = w_host_new
                        contours_attributes[n_index-1][2][3] = h_host_new

                        dots_over_ij_contours_to_draw.append(contours[n_cnt])

                        # Save the attached dot indexes in the contours list
                        dots_over_ij_contours_n_indexes.append(n_cnt)

                        # Erase the attached dot and the old host from the contour map
                        contour_map[y_dot: y_dot + h_dot, x_dot: x_dot + w_dot] = 0
                        contour_map[y_host: y_host + h_host, x_host: x_host + w_host] = 0

                        # Draw the updated host in the contour map
                        contour_map[y1_host_new: y1_host_new + h_host_new, x1_host_new: x1_host_new + w_host_new] = n_index

    if test_output:
        dots_over_ij_contours_img_bgr = plan_img_bgr.copy()
        cv2.drawContours(dots_over_ij_contours_img_bgr, dots_over_ij_contours_to_draw, -1, (255, 0, 255), 1)
        cv2.imwrite(ProjectSettings.root_path + "results/08_dots_over_ij_contours_img_bgr.png", dots_over_ij_contours_img_bgr)

    return dots_over_ij_contours_n_indexes


def adjust_bounding_box_height_by_neighbors(central_n_index, x, y, w, h, contour_map, contours_attributes):
    plan_h, plan_w = contour_map.shape[:2]

    # w_trio = max(4*10, 4*h)    # h_min = 5 for lower-case letters!
    # w_trio = max(2 * 10, 2 * max(w, h))    # h_min = 5 for lower-case letters!
    w_trio = int(3 * max(10, w, h//3))    # h_min = 5 for lower-case letters!
    # w_trio = max(3 * max(10, min(w, h)), 2 * max(w, h))    # h_min = 5 for lower-case letters!
    # w_trio = max(4 * 10, 2 * max(w, h))    # h_min = 5 for lower-case letters!
    x_center = x + w//2
    x1 = max(0, x_center - w_trio//2)
    x2 = min(plan_w - 1, x_center + w_trio//2)
    y1 = y
    y2 = y1 + h - 1
    # if w < 2*h:
    #     # Neither hyphen nor underscore but character (or dot)
    #     y1 = y
    #     y2 = y1 + h - 1
    # else:
    #     # For hyphen and underscore or joint letters :(
    #     y1 = max(0, y - h)
    #     y2 = min(plan_h - 1, y1 + h - 1 + h)

    # t1 = time.time()
    # n_indexes = unique_indexes(contour_map_patch=contour_map[y1: y2+1, x1: x2+1])
    n_indexes = list(np.unique(contour_map[y1: y2+1, x1: x2+1]))
    # t2 = time.time()
    # print('unique_indexes: ' + str(t2 - t1) + ' s')

    left_neighbor_index = -1
    right_neighbor_index = -1

    if n_indexes:
        dx_left, y1_left, y2_left = 1000000, 0, 0
        dx_right, y1_right, y2_right = 1000000, 0, 0
        for n_index in n_indexes:
            if (n_index > 0) and ((n_index-1) != central_n_index):
                x_neighbor, y_neighbor, w_neighbor, h_neighbor = contours_attributes[n_index-1][2]
                x_center_neighbor = x_neighbor + w_neighbor // 2
                if x_center_neighbor <= x_center:
                    # Left neighbor!
                    dx = max(0, x - (x_neighbor + w_neighbor - 1))
                    if dx < dx_left:
                        dx_left, y1_left, y2_left = dx, y_neighbor, y_neighbor + h_neighbor - 1
                        left_neighbor_index = n_index-1
                else:
                    # Right neighbor!
                    dx = max(0, x_neighbor - (x + w - 1))
                    if dx < dx_right:
                        dx_right, y1_right, y2_right = dx, y_neighbor, y_neighbor + h_neighbor - 1
                        right_neighbor_index = n_index-1

        dx_min = min(dx_left, dx_right)
        if 10 < dx_min < 100000:
            dx_acceptable = 2 * dx_min
        else:
            dx_acceptable = 2 * 10

        if dx_left < dx_acceptable:
            y1 = min(y1, y1_left)
            y2 = max(y2, y2_left)
        if dx_right < dx_acceptable:
            y1 = min(y1, y1_right)
            y2 = max(y2, y2_right)

    # Update the height of the bounding box
    y = y1
    h = y2 - y1 + 1
    # And keep the width the same!

    return x, y, w, h, left_neighbor_index, right_neighbor_index


def detect_characters(plan_img_bgr, binary_threshold=160, dots_over_ij=True, test_output=False):
    # t1 = time.time()
    hierarchy, contours, contours_attributes = detect_contours_of_characters(plan_img_bgr, binary_threshold, test_output)
    # print("Number of contours:", len(contours))
    # t2 = time.time()
    # print('detect_contours_of_characters: ' + str(t2 - t1) + ' s')

    # t1 = time.time()
    contour_map = create_contour_map(plan_img_bgr.shape[1], plan_img_bgr.shape[0], contours, contours_attributes, test_output)
    # t2 = time.time()
    # print('create_contour_map: ' + str(t2 - t1) + ' s')

    if dots_over_ij:
        dots_over_ij_contours_n_indexes = attach_dots_over_ij(plan_img_bgr, hierarchy, contour_map, contours, contours_attributes, test_output)
    else:
        dots_over_ij_contours_n_indexes = []

    detections = []

    # t1 = time.time()
    for n_cnt in range(len(contours)):
        x, y, w, h = contours_attributes[n_cnt][2]

        if n_cnt in dots_over_ij_contours_n_indexes:
            detections.append([(x, y, w, h), (x, y, w, h), (0, 0, 0, 0), -1, n_cnt, -1, -2])
        else:
            # Add borders - ???
            # h_img, w_img = plan_img_bgr.shape[:2]
            # border = 1
            # x1 = max(0, x - border)
            # y1 = max(0, y - border)
            # x2 = min(w_img, x + w + border)
            # y2 = min(h_img, y + h + border)
            # x, y, w, h = x1, y1, x2 - x1, y2 - y1

            x_ex, y_ex, w_ex, h_ex, left_n_cnt, right_n_cnt = adjust_bounding_box_height_by_neighbors(n_cnt, x, y, w, h, contour_map, contours_attributes)

            detections.append([(x, y, w, h), (x_ex, y_ex, w_ex, h_ex), (0, 0, 0, 0), left_n_cnt, n_cnt, right_n_cnt, -1])

    # !!!
    fix_broken_connections_to_neighbors(detections)
    # !!!

    # t2 = time.time()
    # print('adjust_bounding_box_height_by_neighbors etc.: ' + str(t2 - t1) + ' s')

    return detections


def fix_broken_connections_to_neighbors(detections):
    num_detections = len(detections)
    for n_det in range(num_detections):
        n_det_left, n_det_center, n_det_right, char_label = detections[n_det][3:]

        if char_label == -2:
            # This is an attached dot!
            continue

        # check connection to the left neighbor
        if n_det_left >= 0:
            # n_det_left_of_left, n_det_center_of_left, n_det_right_of_left, char_label_of_left = detections[n_det_left][3:]
            if detections[n_det_left][-1] != -2 and detections[n_det_left][-2] == -1:
                detections[n_det_left][-2] = n_det_center

        # if detections[n_det_left][-1] != -2 and detections[n_det_left][-2] != n_det_center:
        #     print("Left neighbor ERROR!")

        # check connection to the right neighbor
        if n_det_right >= 0:
            # n_det_left_of_right, n_det_center_of_right, n_det_right_of_right, char_label_of_right = detections[n_det_right][3:]
            if detections[n_det_right][-1] != -2 and detections[n_det_right][-4] == -1:
                detections[n_det_right][-4] = n_det_center

        # if detections[n_det_right][-1] != -2 and detections[n_det_right][-4] != n_det_center:
        #     print("Right neighbor ERROR!")


def main_test():
    plan_img_bgr = cv2.imread("plans/1_3.png", cv2.IMREAD_COLOR)

    t1 = time.time()

    threshold = 160
    detections = detect_characters(plan_img_bgr,
                                   binary_threshold=threshold,
                                   dots_over_ij=True,
                                   test_output=True)

    t2 = time.time()
    print('In total spent ' + str(t2 - t1) + ' s')

    print("Number of detections:", len(detections))

    save_detections_to_json_file_test(detections=detections, json_file_name="ocr_test_results.json")

    draw_detections.check_consistency(detections)

    draw_detections.draw_bounding_boxes(plan_img_bgr,
                                        detections,
                                        draw_cnt_rects=True,
                                        draw_ext_rects=True,
                                        draw_pred_rects=True,
                                        draw_only_valid_labels=False)

    draw_detections.draw_bounding_boxes_with_neighbors(plan_img_bgr, detections, draw_trio_boxes=False)


if __name__ == '__main__':
    main_test()
