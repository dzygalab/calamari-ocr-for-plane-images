import cv2

from settings import ProjectSettings


def check_consistency(detections):
    num_detections = len(detections)

    for n_det in range(num_detections):

        ((x_cnt, y_cnt, w_cnt, h_cnt),
         (x_ext, y_ext, w_ext, h_ext),
         (x_pred, y_pred, w_pred, h_pred),
         n_det_left, n_det_center, n_det_right,
         char_label) = detections[n_det]

        if n_det < 0:
            print("ERROR! n_det < 0:", n_det)

        if n_det_center != n_det:
            print("ERROR! n_det_center != n_det:", n_det_center, "!=", n_det)

        if n_det_left == n_det_center:
            print("ERROR! n_det_left == n_det_center", n_det_left, "==", n_det_center)

        if n_det_center == n_det_right:
            print("ERROR! n_det_center == n_det_right", n_det_center, "==", n_det_right)


def draw_bounding_boxes(plan_img_bgr,
                        detections,
                        draw_cnt_rects=True,
                        draw_ext_rects=True,
                        draw_pred_rects=True,
                        draw_only_valid_labels=False):

    detections_img_bgr = plan_img_bgr.copy()

    num_detections = len(detections)

    for n_det in range(num_detections):

        ((x_cnt, y_cnt, w_cnt, h_cnt),
         (x_ext, y_ext, w_ext, h_ext),
         (x_pred, y_pred, w_pred, h_pred),
         n_det_left, n_det_center, n_det_right,
         char_label) = detections[n_det]

        if draw_ext_rects:
            if w_ext == 0 or h_ext == 0:
                print("ERROR! w_ext == 0 or h_ext == 0")

            cv2.rectangle(img=detections_img_bgr, pt1=(x_ext, y_ext), pt2=(x_ext + w_ext - 1, y_ext + h_ext - 1),
                          color=(255, 0, 0), thickness=1)    # Blue for extended boxes

            # cv2.putText(detections_img_bgr, str(n_det), (x_ex, y_ex + h_ex + 15), 1, 1, (0, 255, 255), 3)
            # cv2.putText(detections_img_bgr, str(n_det), (x_ex, y_ex + h_ex + 15), 1, 1, (0, 0, 0), 1)

        if draw_cnt_rects:
            if w_cnt == 0 or h_cnt == 0:
                print("ERROR! w_cnt == 0 or h_cnt == 0")

            cv2.rectangle(img=detections_img_bgr, pt1=(x_cnt, y_cnt), pt2=(x_cnt + w_cnt - 1, y_cnt + h_cnt - 1),
                          color=(0, 255, 0), thickness=1)    # Green for contour boxes

        if draw_pred_rects and char_label != -1 and char_label != -2:
            if w_pred == 0 or h_pred == 0:
                print("ERROR! w_pred == 0 or h_pred == 0")

            if (char_label is None) and (draw_only_valid_labels is False):
                cv2.rectangle(img=detections_img_bgr, pt1=(x_pred, y_pred),
                              pt2=(x_pred + w_pred - 1, y_pred + h_pred - 1),
                              color=(0, 0, 255), thickness=1)  # Red boxes for unrecognized characters
            else:
                cv2.rectangle(img=detections_img_bgr, pt1=(x_pred, y_pred),
                              pt2=(x_pred + w_pred - 1, y_pred + h_pred - 1),
                              color=(255, 0, 255), thickness=1)  # Magenta for predicted boxes

    cv2.imwrite(ProjectSettings.root_path + "results/detections_rects_img_bgr.png", detections_img_bgr)


def draw_bounding_boxes_with_neighbors(plan_img_bgr, detections, draw_trio_boxes):

    detections_trio_rects_img_bgr = plan_img_bgr.copy()

    num_detections = len(detections)

    for n_det in range(num_detections):

        ((x_cnt, y_cnt, w_cnt, h_cnt),
         (x_ext, y_ext, w_ext, h_ext),
         (x_pred, y_pred, w_pred, h_pred),
         n_det_left, n_det_center, n_det_right,
         char_label) = detections[n_det]

        if n_det_left == -1 and n_det_right == -1:

            cv2.rectangle(img=detections_trio_rects_img_bgr,
                          pt1=(x_ext, y_ext), pt2=(x_ext + w_ext - 1, y_ext + h_ext - 1),
                          color=(0, 0, 255), thickness=1)    # Red for detections without neighbors

        elif n_det_left == -1 and n_det_right >= 0:

            ((x_cnt_of_right, y_cnt_of_right, w_cnt_of_right, h_cnt_of_right),
             (x_ext_of_right, y_ext_of_right, w_ext_of_right, h_ext_of_right),
             (x_pred_of_right, y_pred_of_right, w_pred_of_right, h_pred_of_right),
             n_det_left_of_right, n_det_center_of_right, n_det_right_of_right,
             char_label_of_right) = detections[n_det_right]

            x1_trio = min(x_cnt, x_cnt_of_right)
            y1_trio = min(y_cnt, y_cnt_of_right)
            x2_trio = max(x_cnt + w_cnt - 1, x_cnt_of_right + w_cnt_of_right - 1)
            y2_trio = max(y_cnt + h_cnt - 1, y_cnt_of_right + h_cnt_of_right - 1)

            if draw_trio_boxes:
                cv2.rectangle(img=detections_trio_rects_img_bgr,
                              pt1=(x1_trio, y1_trio), pt2=(x2_trio, y2_trio),
                              color=(255, 0, 128), thickness=1)    # Violet for trio boxes

            cv2.rectangle(img=detections_trio_rects_img_bgr,
                          pt1=(x_ext, y_ext), pt2=(x_ext + w_ext - 1, y_ext + h_ext - 1),
                          color=(255, 0, 0), thickness=1)    # Blue for detections with only right neighbor

        elif n_det_left >= 0 and n_det_right == -1:

            ((x_cnt_of_left, y_cnt_of_left, w_cnt_of_left, h_cnt_of_left),
             (x_ext_of_left, y_ext_of_left, w_ext_of_left, h_ext_of_left),
             (x_pred_of_left, y_pred_of_left, w_pred_of_right, h_pred_of_left),
             n_det_left_of_left, n_det_center_of_left, n_det_right_of_left,
             char_label_of_left) = detections[n_det_left]

            x1_trio = min(x_cnt, x_cnt_of_left)
            y1_trio = min(y_cnt, y_cnt_of_left)
            x2_trio = max(x_cnt + w_cnt - 1, x_cnt_of_left + w_cnt_of_left - 1)
            y2_trio = max(y_cnt + h_cnt - 1, y_cnt_of_left + h_cnt_of_left - 1)

            if draw_trio_boxes:
                cv2.rectangle(img=detections_trio_rects_img_bgr,
                              pt1=(x1_trio, y1_trio), pt2=(x2_trio, y2_trio),
                              color=(255, 0, 128), thickness=1)    # Violet for trio boxes

            cv2.rectangle(img=detections_trio_rects_img_bgr,
                          pt1=(x_ext, y_ext), pt2=(x_ext + w_ext - 1, y_ext + h_ext - 1),
                          color=(0, 255, 0), thickness=1)    # Green for detections with only left neighbor

        elif n_det_left >= 0 and n_det_right >= 0:

            ((x_cnt_of_left, y_cnt_of_left, w_cnt_of_left, h_cnt_of_left),
             (x_ext_of_left, y_ext_of_left, w_ext_of_left, h_ext_of_left),
             (x_pred_of_left, y_pred_of_left, w_pred_of_right, h_pred_of_left),
             n_det_left_of_left, n_det_center_of_left, n_det_right_of_left,
             char_label_of_left) = detections[n_det_left]

            ((x_cnt_of_right, y_cnt_of_right, w_cnt_of_right, h_cnt_of_right),
             (x_ext_of_right, y_ext_of_right, w_ext_of_right, h_ext_of_right),
             (x_pred_of_right, y_pred_of_right, w_pred_of_right, h_pred_of_right),
             n_det_left_of_right, n_det_center_of_right, n_det_right_of_right,
             char_label_of_right) = detections[n_det_right]

            x1_trio = min(x_cnt, x_cnt_of_left, x_cnt_of_right)
            y1_trio = min(y_cnt, y_cnt_of_left, y_cnt_of_right)
            x2_trio = max(x_cnt + w_cnt - 1, x_cnt_of_left + w_cnt_of_left - 1, x_cnt_of_right + w_cnt_of_right - 1)
            y2_trio = max(y_cnt + h_cnt - 1, y_cnt_of_left + h_cnt_of_left - 1, y_cnt_of_right + h_cnt_of_right - 1)

            if draw_trio_boxes:
                cv2.rectangle(img=detections_trio_rects_img_bgr,
                              pt1=(x1_trio, y1_trio), pt2=(x2_trio, y2_trio),
                              color=(255, 0, 128), thickness=1)    # Violet for trio boxes

            cv2.rectangle(img=detections_trio_rects_img_bgr,
                          pt1=(x_ext, y_ext), pt2=(x_ext + w_ext - 1, y_ext + h_ext - 1),
                          color=(255, 0, 255), thickness=1)    # Magenta for detections with only left neighbor

        else:
            print("ERROR! Unexpected value for neighbors")
            exit(1001)

    cv2.imwrite(ProjectSettings.root_path + "results/detections_trio_rects_img_bgr.png", detections_trio_rects_img_bgr)
